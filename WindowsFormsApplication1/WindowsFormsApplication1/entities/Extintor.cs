﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1.entities
{
    class Extintor
    {
        private int id;
        private categoria categoria;
        private TipoExtintor tipo;
        private Marca marca;
        private Capacidad capacidad;
        private UnidadMedida medida;
        private string lugarDesignado;
        private DateTime fecha_recarga;

        public Extintor(int id, categoria categoria, TipoExtintor tipo, Marca marca, Capacidad capacidad, UnidadMedida medida, string lugarDesignado, DateTime fecha_recarga)
        {
            this.id = id;
            this.categoria = categoria;
            this.tipo = tipo;
            this.marca = marca;
            this.capacidad = capacidad;
            this.medida = medida;
            this.lugarDesignado = lugarDesignado;
            this.fecha_recarga = fecha_recarga;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        internal categoria Categoria
        {
            get
            {
                return categoria;
            }

            set
            {
                categoria = value;
            }
        }

        internal TipoExtintor Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
                tipo = value;
            }
        }

        internal Marca Marca
        {
            get
            {
                return marca;
            }

            set
            {
                marca = value;
            }
        }

        internal Capacidad Capacidad
        {
            get
            {
                return capacidad;
            }

            set
            {
                capacidad = value;
            }
        }

        internal UnidadMedida Medida
        {
            get
            {
                return medida;
            }

            set
            {
                medida = value;
            }
        }

        public string LugarDesignado
        {
            get
            {
                return lugarDesignado;
            }

            set
            {
                lugarDesignado = value;
            }
        }

        public DateTime Fecha_recarga
        {
            get
            {
                return fecha_recarga;
            }

            set
            {
                fecha_recarga = value;
            }
        }
    }
    enum categoria
    {
        Americano, Europeo
    }

    enum TipoExtintor
    {
        Agua,Espuma_AFFF,Polvo_ABC,CO2
    }
    enum Marca
    {
        Tornado, Amerex, otros
    }
    enum Capacidad
    {
        Cinco = 5, Diez = 10, Veinte = 20, Cincuenta = 50
    }
    enum UnidadMedida
    {
        Kilogramos, Libras, Litros
    }
}
