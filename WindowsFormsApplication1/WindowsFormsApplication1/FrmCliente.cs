﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drClientes;
        public FrmCliente()
        {
            InitializeComponent();
        }

        public DataRow DrClientes
        {
            set
            {
                drClientes = value;
                txtId.Text = drClientes["Id"].ToString();
                txtCedula.Text = drClientes["Cedula"].ToString();
                txtNombre.Text = drClientes["Nombre"].ToString();
                txtApellido.Text = drClientes["Apellido"].ToString();
                txtTelefono.Text = drClientes["Telefono"].ToString();
                txtCorreo.Text = drClientes["Correo"].ToString();
                txtDireccion.Text = drClientes["Direccion"].ToString();
                txtMunicipio.Text = drClientes["Municipio"].ToString();
                txtDepartamento.Text = drClientes["Departamento"].ToString();
            }
        }

        public DataTable TblClientes
        {
            get
            {
                return tblClientes;
            }

            set
            {
                tblClientes = value;
            }
        }

        public DataSet DsClientes
        {
            get
            {
                return dsClientes;
            }

            set
            {
                dsClientes = value;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(txtId.Text);
            string cedula = txtCedula.Text;
            string nombre = txtNombre.Text;
            string apellido = txtApellido.Text;
            string telefono = txtTelefono.Text;
            string correo = txtCedula.Text;
            string direccion = txtDireccion.Text;
            string municipio = txtMunicipio.Text;
            string departamento = txtDepartamento.Text;

            if (drClientes != null)
            {
                DataRow drNew = TblClientes.NewRow();

                int index = TblClientes.Rows.IndexOf(drClientes);
                drNew["Id"] = drClientes["Id"];
                drNew["Cedula"] = cedula;
                drNew["Nombre"] = nombre;
                drNew["Apellido"] = apellido;
                drNew["Telefono"] = telefono;
                drNew["Correo"] = correo;
                drNew["Direccion"] = direccion;
                drNew["Municipio"] = municipio;
                drNew["Departamento"] = departamento;

                TblClientes.Rows.RemoveAt(index);
                TblClientes.Rows.InsertAt(drNew, index);

            }
            else
            {
                TblClientes.Rows.Add(TblClientes.Rows.Count + 1, cedula, nombre, apellido, telefono, correo,direccion,municipio,departamento);
            }

            Dispose();


        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Cliente"].TableName;
        }
    }
}
