﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1.model;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();               
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionCliente fgp = new FrmGestionCliente();
            fgp.MdiParent = this;
            fgp.DsClientes = dataSetSistema;
            fgp.Show();
        }

        private void extintoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGestionExtintor fge = new FrmGestionExtintor();
            fge.MdiParent = this;
            fge.DsExtintors1 = dataSetSistema;
            fge.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ClienteModel.populate();


        }
    }
}
